//
//  GiuseppeViewController.swift
//  ReMemos-iOS
//
//  Created by Giuseppe Caccavale on 18/10/2018.
//  Copyright © 2018 Giuseppe Caccavale. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class GiuseppeViewController: UIViewController {
    
    @IBOutlet weak var eventTextFiled: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func addMemoButton(_ sender: UIButton) {
    
        let eventString: String = eventTextFiled.text!
        let yearString: String = yearTextField.text!
        
        if eventString.isEmpty || yearString.isEmpty || Int(yearString) == nil{
            if eventString.isEmpty{
                eventTextFiled.layer.borderWidth = 1
                eventTextFiled.layer.borderColor = UIColor.red.cgColor
                print("TextField empty")
            }
            else {
                eventTextFiled.layer.borderWidth = 0
            }
            
            if yearString.isEmpty{
                yearTextField.layer.borderWidth = 1
                yearTextField.layer.borderColor = UIColor.red.cgColor
                print("TextField empty")
            }
            else {
                yearTextField.layer.borderWidth = 0
            }
            
            if Int(yearString) == nil{
                yearTextField.layer.borderWidth = 1
                yearTextField.layer.borderColor = UIColor.red.cgColor
                print("Invalid Year")
            }
            else {
                yearTextField.layer.borderWidth = 0
            }
        }
            
        else {
            eventTextFiled.layer.borderWidth = 0
            yearTextField.layer.borderWidth = 0
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "User", in: context)
            let newData = NSManagedObject(entity: entity!, insertInto: context)
            
        
            newData.setValue(eventTextFiled.text, forKey: "event")
            newData.setValue(yearTextField.text, forKey: "year")
            
            do {
                try context.save()
            }
            catch {
                print("Failed saving")
            }
            /*
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            //request.predicate = NSPredicate(format: "age = %@", "12")
            request.returnsObjectsAsFaults = false
            
            do {
                let result = try context.fetch(request)
                for data in result as! [NSManagedObject] {
                    print("Event: \(data.value(forKey: "event") as! String)")
                    print("Year: \(data.value(forKey: "year") as! String)")
                }
                
            } catch {
                
                print("Failed")
            }*/
        }
    }
    
}
