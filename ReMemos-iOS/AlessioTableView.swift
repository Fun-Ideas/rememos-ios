//
//  AlessioTableViewTableViewController.swift
//  ReMemos-iOS
//
//  Created by Giuseppe Caccavale on 19/10/2018.
//  Copyright © 2018 Giuseppe Caccavale. All rights reserved.
//

import UIKit
import CoreData

class AlessioTableView: UITableViewController {

    var frasi = [String]()
    var Years = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDatabase()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadDatabase()
        self.tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frasi.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Row = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) // Per risparmiare memoria conviene utilizzare il metodo .dequeueReusableCell
        Row.textLabel?.text = frasi[indexPath.row]
        return Row
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print(indexPath.row)
        performSegue(withIdentifier: "check", sender: Years[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "check" {
            let vc = segue.destination as! AlfonsoViewController
            vc.trueAnswerString = sender as? NSString
        }
    }
    
    func loadDatabase(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            frasi.removeAll()
            Years.removeAll()
            for data in result as! [NSManagedObject] {
                //print(data.value(forKey: "event") as! String)
                frasi.append(data.value(forKey: "event") as! String)
                Years.append(data.value(forKey: "year") as! String)
            }
            print(frasi)
            print(Years)
            
        } catch {
            
            print("Failed")
        }
    }
    
}
