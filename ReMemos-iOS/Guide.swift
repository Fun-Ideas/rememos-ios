//
//  Guide.swift
//  Rememos
//
//  Created by Erzhena Mironova on 17/10/2018.
//  Copyright © 2018 Erzhena Mironova. All rights reserved.
//

import UIKit

class Guide: NSObject {
    
    let id: Int
    let title: String
    let subtitle: String
    let body: String
    
    init(id: Int, title: String, subtitle: String, body: String) {
        self.id = id
        self.title = title
        self.subtitle = subtitle
        self.body = body
    }

}
