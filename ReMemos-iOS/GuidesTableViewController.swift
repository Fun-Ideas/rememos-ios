//
//  GuidesTableViewController.swift
//  Rememos
//
//  Created by Erzhena Mironova on 17/10/2018.
//  Copyright © 2018 Erzhena Mironova. All rights reserved.
//

import UIKit

class GuidesTableViewController: UITableViewController{
    
    let guides = [
                Guide(id: 1, title: "Creativity is the best way to memorise", subtitle: "Tips", body: "Начните заниматься с тренерами мирового уровня, каждый из которых обладает статусом «Мастер‑тренер Nike». Они подготовили более 180 бесплатных тренировок, состоящих из разных комплексов по фитнесу, бегу и не только. Кроме того, приложение предложит подходящую тренировку, чтобы вы могли быстрее закрыть кольцо «Упражнения». Начните заниматься с тренерами мирового уровня, каждый из которых обладает статусом «Мастер‑тренер Nike». Они подготовили более 180 бесплатных тренировок, состоящих из разных комплексов по фитнесу, бегу и не только. Кроме того, приложение предложит подходящую тренировку, чтобы вы могли быстрее закрыть кольцо «Упражнения». Начните заниматься с тренерами мирового уровня, каждый из которых обладает статусом «Мастер‑тренер Nike». Они подготовили более 180 бесплатных тренировок, состоящих из разных комплексов по фитнесу, бегу и не только. Кроме того, приложение предложит подходящую тренировку, чтобы вы могли быстрее закрыть кольцо «Упражнения». Начните заниматься с тренерами мирового уровня, каждый из которых обладает статусом «Мастер‑тренер Nike». Они подготовили более 180 бесплатных тренировок, состоящих из разных комплексов по фитнесу, бегу и не только. Кроме того, приложение предложит подходящую тренировку, чтобы вы могли быстрее закрыть кольцо «Упражнения». Начните заниматься с тренерами мирового уровня, каждый из которых обладает статусом «Мастер‑тренер Nike». Они подготовили более 180 бесплатных тренировок, состоящих из разных комплексов по фитнесу, бегу и не только. Кроме того, приложение предложит подходящую тренировку, чтобы вы могли быстрее закрыть кольцо «Упражнения». Начните заниматься с тренерами мирового уровня, каждый из которых обладает статусом «Мастер‑тренер Nike». Они подготовили более 180 бесплатных тренировок, состоящих из разных комплексов по фитнесу, бегу и не только. Кроме того, приложение предложит подходящую тренировку, чтобы вы могли быстрее закрыть кольцо «Упражнения»."),
                Guide(id: 2, title: "Learn more every day", subtitle: "Productivity", body: "rhfkhfkdhgdfkghdfgkdgdfglkdfkghdfhgdfgldfkghdfghdfk"),
                Guide(id: 3, title: "Just title", subtitle: "Tips", body: "rhfkhfkdhgdfkghdfgkdgdfglkdfkghdfhgdfgldfkghdfghdfk")
                  ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guides.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "guideIdentifier", for: indexPath)
        
        let guide = guides[indexPath.row]
        
        cell.textLabel?.text = guide.title
        cell.detailTextLabel?.text = guide.subtitle
        
        return cell
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let guide = guides[tableView.indexPathForSelectedRow!.row]
        
        let guideViewController = segue.destination as! GuideViewController
        guideViewController.guide = guide
    }
}
