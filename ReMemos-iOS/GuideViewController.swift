//
//  GuideViewController.swift
//  Rememos
//
//  Created by Erzhena Mironova on 17/10/2018.
//  Copyright © 2018 Erzhena Mironova. All rights reserved.
//

import UIKit

class GuideViewController: UIViewController {
    
    @IBOutlet weak var guideTitle: UILabel!
    @IBOutlet weak var guideText: UILabel!
    
    var guide: Guide?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let guideInfo = guide {
            guideTitle.text = guideInfo.title
            guideText.text = guideInfo.body
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
