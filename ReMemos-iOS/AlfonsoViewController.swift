//
//  AlfonsoViewController.swift
//  ReMemos-iOS
//
//  Created by Giuseppe Caccavale on 18/10/2018.
//  Copyright © 2018 Giuseppe Caccavale. All rights reserved.
//

import Foundation
import UIKit

class AlfonsoViewController: UIViewController {
    
    var trueAnswerString: NSString?
    var trueAnswer = 1991
    var mixed_array = [Int]()
    var ans = Int()
    
    @IBOutlet weak var date1: UIButton!
    @IBOutlet weak var date2: UIButton!
    @IBOutlet weak var date3: UIButton!
    @IBOutlet weak var date4: UIButton!
    
    @IBOutlet weak var check: UIButton!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if trueAnswerString != "" {
            trueAnswer = (trueAnswerString)!.integerValue
        }
        
        date1.layer.cornerRadius = 10
        date1.layer.borderWidth = 1
        date1.layer.borderColor = UIColor.gray.cgColor
        date2.layer.cornerRadius = 10
        date2.layer.borderWidth = 1
        date2.layer.borderColor = UIColor.gray.cgColor
        date3.layer.cornerRadius = 10
        date3.layer.borderWidth = 1
        date3.layer.borderColor = UIColor.gray.cgColor
        date4.layer.cornerRadius = 10
        date4.layer.borderWidth = 1
        date4.layer.borderColor = UIColor.gray.cgColor
        check.layer.cornerRadius = 10
        
        
        mixed_array = RandomDate().shuffled()
        
        date1.setTitle(String(mixed_array[0]), for: .normal)
        date2.setTitle(String(mixed_array[1]), for: .normal)
        date3.setTitle(String(mixed_array[2]), for: .normal)
        date4.setTitle(String(mixed_array[3]), for: .normal)
        
    }
    
    @IBAction func date1(_ sender: UIButton) {
        date1.backgroundColor = UIColor.gray
        date1.setTitleColor(UIColor.white, for: .normal)
        ans = mixed_array[0]
    }
    
    @IBAction func date2(_ sender: UIButton) {
        date2.backgroundColor = UIColor.gray
        date2.setTitleColor(UIColor.white, for: .normal)
        ans = mixed_array[1]
    }
    
    @IBAction func date3(_ sender: UIButton) {
        date3.backgroundColor = UIColor.gray
        date3.setTitleColor(UIColor.white, for: .normal)
        ans = mixed_array[2]
    }
    @IBAction func date4(_ sender: UIButton) {
        date4.backgroundColor = UIColor.gray
        date4.setTitleColor(UIColor.white, for: .normal)
        ans = mixed_array[3]
    }
    
    
    @IBAction func check(_ sender: UIButton) {
        if(ans == trueAnswer) {
            //print("risposta esatta") //variabile esatta
            performSegue(withIdentifier: "result", sender: true)
        }
        else {
            //print("risposta sbagliata") //variabile sbagliata
            performSegue(withIdentifier: "result", sender: false)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "result" {
            let vc = segue.destination as! GameFinalScreen
            vc.result = sender as! Bool
        }
    }
    
    func RandomDate() -> Array<Int>{
        var date = [Int]()
        var count = 0
        while count < 3 {
            let element = Int.random(in: 1300..<2019)
            if element != trueAnswer {
                date.append(element)
                count += 1
            }
        }
        date.append(trueAnswer)
        return date
    }
        
    
    
}
