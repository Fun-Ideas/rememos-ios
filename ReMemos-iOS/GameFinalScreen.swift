//
//  GameFinalScreen.swift
//  ReMemos-iOS
//
//  Created by Alessio Di Nardo on 18/10/2018.
//  Copyright © 2018 Giuseppe Caccavale. All rights reserved.
//

import Foundation
import UIKit

class GameFinalScreen: UIViewController {
    
    @IBOutlet weak var Score: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var imageResult: UIImageView!
    
    var result = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Risposta: \(result)")
        
        if result {
            Score.text = "CONGRATULATIONS!"
            retryButton.setTitle("Add a New Memo", for: .normal)
            imageResult.image = UIImage(named:"trueAnswer.jpg")
        }
        else {
            Score.text = "GOOD LUCK NEXT TIME!"
            retryButton.setTitle("Retry", for: .normal)
            imageResult.image = UIImage(named:"falseAnswer.jpg")
        }
    }
    
    @IBAction func retryButton(_ sender: UIButton) {
        if result{
            performSegue(withIdentifier: "newMemo", sender: nil)
        }
    }
    
}
