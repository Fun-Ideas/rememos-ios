//
//  ViewController.swift
//  ReMemos-iOS
//
//  Created by Fun Ideas on 17/10/2018.
//  Copyright © 2018 Fun Ideas. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    @IBOutlet weak var createdMemosLabel: UILabel!
    @IBOutlet weak var learnedMemosLabel: UILabel!
    @IBOutlet weak var successRateLabel: UILabel!
    @IBOutlet weak var percentageLearnedLabel: UILabel!
    @IBOutlet weak var percentageSuccessLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Replace hardcoded values
        let createdMemos: Double = 200 // total number of memos
        let learnedMemos: Double = 130 // memos which has at least one successful check
        let totalAttemps: Double = 180 // sum of all checks
        let successAttempts: Double = 100 // attempts which are marked auccessful
        
        createdMemosLabel.text = String(Int(createdMemos))
        learnedMemosLabel.text = String(Int(learnedMemos))
        successRateLabel.text = "\(Int(successAttempts))/\(Int(totalAttemps))"
        let learnedRate = Int(learnedMemos/createdMemos*100)
        percentageLearnedLabel.text = "\(learnedRate)%"
        percentageSuccessLabel.text = "\(Int(successAttempts/totalAttemps*100))%"
        levelLabel.text = getLevel(learnedMemos)
        
    }
    
    func getLevel(_ learnedMemosCount:Double) -> String {
        switch learnedMemosCount {
        case 0...50:
            return "Baby"
        case 51...99:
            return "Student"
        case 100...100000:
            return "Professor"
        default:
            return "Unknown"
        }
    }
}

